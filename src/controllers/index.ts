import { Router } from 'express'
import path from 'path'
import fs from 'fs'
export default () => {
  const routes: Array<Router> = []
  fs.readdirSync(path.join(__dirname, '')).forEach((file: string) => {
    const excludeList: Array<string> = ['index.js', 'index.ts']
    if (excludeList.indexOf(file) < 0) {
      const controllers = require(path.join(__dirname, file)).default()
      routes.push(controllers)
    }
  })
  return routes
}
