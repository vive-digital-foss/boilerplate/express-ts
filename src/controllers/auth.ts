import { Router, Request, Response, NextFunction } from 'express'
import { check, validationResult } from 'express-validator'
import { UserService } from '../services/user'
import { UserModel, UserDocument } from '../models/User'
import { UnprocessableEntityError } from '../utils/errors'

export default () => {
  const api: Router = Router()
  api.post(
    '/auth/signin',
    [
      check('username')
        .not()
        .isEmpty(),
      check('password')
        .not()
        .isEmpty()
    ],
    (req: Request, res: Response, next: NextFunction) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return next(new UnprocessableEntityError('ValidationError', errors.array()))
      }
      const userService = new UserService()
      userService
        .authenticate(req.body.username, req.body.password)
        .then(userData => {
          return res.json(userData)
        })
        .catch(err => {
          return next(err)
        })
    }
  )

  api.post(
    '/auth/signup',
    [
      check('username')
        .not()
        .isEmpty(),
      check('password')
        .not()
        .isEmpty(),
      check('password_confirm')
        .not()
        .isEmpty()
    ],
    (req: Request, res: Response, next: NextFunction) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return next(new UnprocessableEntityError('ValidationError', errors.array()))
      }
      const userService = new UserService()
      const userModel: UserModel = { username: req.body.username, password: req.body.password }
      userService
        .create(userModel)
        .then((doc: UserDocument) => {
          return res.json(doc)
        })
        .catch(err => {
          return next(err)
        })
    }
  )

  return api
}
