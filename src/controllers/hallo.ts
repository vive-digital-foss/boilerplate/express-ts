import { Router } from 'express'

export default () => {
  const api: Router = Router()
  api.get('/hallo', (req, res) => {
    return res.json('hallo API')
  })
  return api
}
