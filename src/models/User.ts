import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
export enum UserStatus {
  Active = 'active',
  Inactive = 'inactive'
}
export type UserModel = {
  username: string
  password: string
  email?: string
  status?: UserStatus
  latestLogin?: Date
  updatedAt?: Date
  createdAt?: Date
}

export type UserDocument = mongoose.Document & UserModel

const Schema = mongoose.Schema
const userSchema = new Schema(
  {
    username: {
      type: String,
      lowercase: true,
      unique: true,
      required: true,
      validate: (username: string) => {
        return (
          /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(username) ||
          /\d{10}/.test(username)
        )
      }
    },
    password: {
      type: String,
      required: true
    },
    email: {
      type: String
    },
    status: {
      type: String,
      default: 'active'
    },
    latestLogin: {
      type: Date,
      default: Date.now
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    createdAt: {
      type: Date,
      default: Date.now
    }
  },
  { collection: 'users' }
)

userSchema.pre('save', function(next) {
  const user = this
  if (this.isModified('password') || this.isNew) {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(user.get('password'), salt)
    user.set('password', hash)
    next()
  } else {
    return next()
  }
})

const User = mongoose.model('User', userSchema)
export default User
