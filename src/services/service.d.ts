export interface ServiceInterface {
  create(model: any): any
  update(id: string, model: any): any
  delete(id: string): any
  getById(id: string): any
  getAll(criteria: any, limit: number, offset: number): any
}
