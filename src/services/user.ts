import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import { ServiceInterface } from './service'
import User, { UserModel, UserDocument } from '../models/User'
import { JWT_SECRET } from '../utils/secrets'
import { InternalServerError, NotFoundError, UnauthorizedError, ModelError } from '../utils/errors'
import logger from '../utils/logger'

export class UserService implements ServiceInterface {
  constructor() {}
  authenticate(username: string, password: string) {
    logger.debug(`receive username: ${username}`)
    return new Promise((resolve, reject) => {
      this.getByUsername(username)
        .then((doc: UserDocument) => {
          if (doc === null) {
            reject(new NotFoundError('user not found'))
          }
          if (bcrypt.compareSync(password, doc.password)) {
            const userData = doc.toJSON()
            delete userData.password
            const token = jwt.sign(userData, JWT_SECRET, {
              expiresIn: '60days'
            })
            Object.assign(userData, { token: token })
            return resolve(userData)
          } else {
            return reject(new UnauthorizedError('password mismatch'))
          }
        })
        .catch((err: Error) => {
          return reject(ModelError(err))
        })
    })
  }
  create(model: UserModel) {
    logger.debug('user.create(model)', model)
    return new Promise((resolve, reject) => {
      User.create(model)
        .then((doc: UserDocument) => {
          resolve(doc)
        })
        .catch(err => {
          reject(ModelError(err))
        })
    })
  }
  update(id: string, model: any) {
    logger.debug(`user.update(${id}, model)`, model)
    return new Promise((resolve, reject) => {
      User.update({ _id: id }, { $set: model })
        .then(() => {
          this.getById(id)
            .then((doc: UserDocument) => {
              resolve(doc)
            })
            .catch(err => reject(ModelError(err)))
        })
        .catch(err => reject(ModelError(err)))
    })
  }
  delete(id: string) {
    logger.debug(`user.delete(${id})`)
  }
  getById(id: string): Promise<UserDocument> {
    logger.debug(`user.getById(${id})`)
    return new Promise((resolve, reject) => {
      User.findById(id)
        .select('-password')
        .populate('Businesss')
        .populate('Profile')
        .then((doc: UserDocument) => {
          resolve(doc)
        })
        .catch(err => {
          reject(ModelError(err))
        })
    })
  }
  getByUsername(username: string) {
    return new Promise((resolve, reject) => {
      User.findOne({ username: username })
        .then((doc: UserDocument) => {
          resolve(doc)
        })
        .catch((err: Error) => {
          reject(ModelError(err))
        })
    })
  }
  getAll(criteria: any, limit: number, offset: number) {}
}
