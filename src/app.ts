import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import dotenv from 'dotenv'
import mongoose from 'mongoose'

import logger from './utils/logger'
import { MONGODB_URI } from './utils/secrets'
import * as authorizeMiddleware from './middleware/authorize'
import defaultControllers from './controllers'
class App {
  public app: express.Application
  constructor() {
    this.app = express()
    this.config()
    this.registerMongoDB()
    this.registerRoutes()
  }

  private config(): void {
    // config
    this.app.set('port', process.env.PORT || 3000)
    dotenv.config({ path: '.env.example' })
    // middleware
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(bodyParser.json())
    this.app.use(cors())
  }

  private async registerMongoDB(): Promise<void> {
    const mongodbURI = MONGODB_URI
    mongoose.Promise = Promise
    try {
      await mongoose.connect(mongodbURI, { useNewUrlParser: true })
    } catch (err) {
      logger.error(err)
      process.exit(1)
    }
  }

  private registerRoutes(): void {
    this.app.use('/', authorizeMiddleware.isAuthorized, defaultControllers())
  }
}

export default new App().app
