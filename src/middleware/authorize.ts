import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import { JWT_SECRET } from '../utils/secrets'

declare global {
  namespace Express {
    export interface Request {
      user?: string | object
    }
  }
}
const byPassAuthURLs = ['/auth/signin', '/auth/signup', '/hallo', '/reset']
export let isAuthorized = (req: Request, res: Response, next: NextFunction) => {
  if (byPassAuthURLs.indexOf(req.url) !== -1) {
    return next()
  }
  let token: string = req.headers['authorization']
  if (token) {
    token = token.replace('Bearer ', '')
    jwt.verify(token, JWT_SECRET, (err: Error, decoded) => {
      if (err) {
        return res.status(401).json(err)
      }
      req.user = decoded
      next()
    })
  } else {
    return res.status(401).json({
      message: 'no token provided'
    })
  }
}
