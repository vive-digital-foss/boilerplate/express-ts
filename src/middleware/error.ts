import { Request, Response, NextFunction, ErrorRequestHandler } from 'express'
import logger from '../utils/logger'
import { ApplicationError } from '../utils/errors'
export default (err: ApplicationError, req: Request, res: Response, next: NextFunction) => {
  logger.error(err)
  res.status(err.statusCode).json(err)
}
