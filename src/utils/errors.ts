import logger from './logger'
export class ApplicationError extends Error {
  public statusCode: number
  public payload: any
  constructor(message: string, statusCode: number, payload?: any) {
    super()
    Error.captureStackTrace(this, this.constructor)

    this.name = this.constructor.name

    this.message = message || 'Something went wrong. Please try again.'

    this.statusCode = statusCode || 500
    if (payload !== undefined) {
      this.payload = payload
    }
  }
}

// 4xx Client Error
export class BadRequestError extends ApplicationError {
  constructor(message: string) {
    super(message, 400)
  }
}

export class UnauthorizedError extends ApplicationError {
  constructor(message: string) {
    super(message, 401)
  }
}
export class NotFoundError extends ApplicationError {
  constructor(message: string) {
    super(message, 404)
  }
}

export class UnprocessableEntityError extends ApplicationError {
  constructor(message: string, payload?: any) {
    super(message, 422, payload)
  }
}

// 5xx Server Error
export class InternalServerError extends ApplicationError {
  constructor(message: string) {
    super(message, 500)
  }
}

export class NotImplementError extends ApplicationError {
  constructor(message: string) {
    super(message, 501)
  }
}

// Special Error

export let ModelError = (err: any) => {
  logger.error(err)
  if (err.name === 'ValidationError') {
    return new UnprocessableEntityError(err.message)
  } else {
    return new InternalServerError(err.message)
  }
}
