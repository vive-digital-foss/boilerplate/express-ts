import winston from 'winston'
import { createLogger } from 'winston'
import { ENVIRONMENT } from './secrets'

const logLevel: string = 'debug'

const logger = createLogger({
  transports: [
    new winston.transports.Console({
      level: process.env.NODE_ENV === 'production' ? 'error' : 'debug'
    }),
    new winston.transports.File({ filename: 'debug.log', level: 'debug' }),
    new winston.transports.File({ filename: 'error.log', level: 'error' })
  ]
})

if (process.env.NODE_ENV !== 'production') {
  logger.debug('Logging initialized at debug level')
}

export default logger
