# Express TS Boilerplate

Boilerplate for create REST API Server with Jest and MongoDB Adapter

# Usage
```
$ yarn install
$ yarn run dev
```

# Build
```
$ yarn run build
```
